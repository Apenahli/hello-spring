package az.ingress.controller;

import az.ingress.dto.StudentDto;
import az.ingress.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/student")
public class StudentController {

    private final StudentService studentService;

    @PostMapping
    public ResponseEntity<?>createStudent(@RequestBody StudentDto studentDto){
        return ResponseEntity.ok().body(studentService.createStudent(studentDto));
    }
    @GetMapping("/{id}")
    public ResponseEntity<?>getStudentById(@PathVariable int id){
        return ResponseEntity.ok().body(studentService.findStudentById(id));
    }
    @PutMapping("/{id}")
    public ResponseEntity<?>updateStudent(@PathVariable int id,@RequestBody StudentDto studentDto){
        return ResponseEntity.ok().body(studentService.updateStudentById(id,studentDto));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?>deleteStudent(@PathVariable int id){
        studentService.deleteStudentById(id);
        return ResponseEntity.ok().build();
    }
    @GetMapping
    public ResponseEntity<List<?>>getStudents(){
        return ResponseEntity.ok().body(studentService.getStudents());
    }

}
