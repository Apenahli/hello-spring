package az.ingress.service;

import az.ingress.dto.StudentDto;
import az.ingress.entity.Student;
import az.ingress.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class StudentService {

    private final StudentRepository studentRepository;

    public StudentDto createStudent(StudentDto studentDto) {
        Student student = Student.builder().name(studentDto.getName()).
                institute(studentDto.getInstitute()).birthdate(studentDto.getBirthdate()).build();
        return getStudentDto(studentRepository.save(student));
    }

    public StudentDto findStudentById(int id) {
        Student student = studentRepository
                .findById(id).orElseThrow(() ->
                        new RuntimeException("Student not found with this id: "+id));
        return getStudentDto(student);
    }

    public StudentDto updateStudentById(int id, StudentDto studentDto) {
        findStudentById(id);
        Student student = Student.builder().id(id).name(studentDto.getName()).
                institute(studentDto.getInstitute()).birthdate(studentDto.getBirthdate()).build();
        return getStudentDto(studentRepository.save(student));
    }

    public void deleteStudentById(int id) {
        findStudentById(id);
        studentRepository.deleteById(id);
    }

    public List<StudentDto> getStudents() {
        List<Student> students = studentRepository.findAll();
        if (students.isEmpty()) throw new RuntimeException("Student List is empty");
        List<StudentDto> studentsDto = new ArrayList<>();
        for (Student student : students) { studentsDto.add(getStudentDto(student)); }
        return studentsDto;
    }

    private StudentDto getStudentDto(Student student) {
        return StudentDto.builder().id(student.getId()).name(student.getName())
                .institute(student.getInstitute()).birthdate(student.getBirthdate()).build();
    }

}
